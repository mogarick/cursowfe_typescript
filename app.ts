//Import

import Person from "./Persona";

var aPerson = new Person("Ricardo","Antonio","mogarick@gmail.com",1979);
//aPerson.miVariablePrivada = "Si te veo";
// console.log("aPerson.miVariablePrivada",aPerson.miVariablePrivada);
//OJO: No está modificando miVariablePrivada, 
//sino definiendo la variable miVariablePrivada para la instancia aPerson
// aPerson.miVariablePrivada = "Si te veo";
// console.log("aPerson.miVariablePrivada",aPerson.miVariablePrivada);

var anotherPerson = new Person("Ramiro","Rosales","coprreo@gmail.com",1989);
// console.log("anotherPerson.miVariablePrivada",anotherPerson.miVariablePrivada);

console.log("aPerson.miVariablePrivada",aPerson.verMiVariablePrivada());
console.log("anotherPerson.miVariablePrivada",anotherPerson.verMiVariablePrivada());