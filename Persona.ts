var miVariablePrivada = "No me ven";    


export default class Persona {
    
    constructor(
        private nombre:string,private apellidos:string,private correo:string,private aNacimiento:number){
        // this.nombre = nombre;
        // this.apellidos = apellidos;
        // this.correo = correo;
        // this.aNacimiento = aNacimiento;
    }

    calcularEdad(){
        return new Date().getFullYear() - this.aNacimiento;
    }

    verMiVariablePrivada(){
        return miVariablePrivada + " a simple vista ;)";
    }

}



