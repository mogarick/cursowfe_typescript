//06 Destructuring
//arreglos 

// var miArreglo = [123,"Desayunar",false];

// let id, nomb, terminado;

// for(let c = 0; c<= miArreglo.length; c++){
//   if(c == 0) {
//     id = miArreglo[c];
//   } else if(c == 1) {
//     nomb = miArreglo[c];
//   } else if (c == 2) {
//     terminado = miArreglo[c];
//   }

// }

// console.log("id=", id);
// console.log("nomb=", nomb);
// console.log("terminado=", terminado);


// var [ID,Nombre,Terminado] = miArreglo;

// console.log("Valores extraidos de arreglo:",ID,Nombre,Terminado);

//13:00


// var part1 = "Ricardo";
// var part2 = "Montoya";

// [part1,part2] = [part2,part1];

// console.log(part1,part2);


//objetos
// var tarea = {
//     id:123,
//     nombre:"Bañarse",
//     terminado:false
// }

// // var {id,terminado,nombre} = tarea;

// var {id, terminado:finalizado, nombre} = tarea;



// console.log("Valores extraidos de objeto:",id,finalizado,nombre);

// //OJO:Si el objeto no contiene el valor, manda error.
// //El no poner id:id es también un feature de ES6
// //var {id,terminado,nombre,createdAt} = tarea;

// function getTarea(id){
//     var todo = {
//         id:124,
//         nombre: "Almorzar",
//         terminado:false
//     }

//     return todo
// }

// var {id,terminado,nombre} = getTarea(124);

// //Asignando a variables con nombre diferente de la propiedad del Obj
// var {id:myId,terminado:isDone,nombre:nombreTarea} = getTarea(124);
// console.log("Valores extraidos de objeto, con nomvre de var distinto:",myId,isDone,nombreTarea);

//Uso en parámetros de funciones
// function printTarea({id,nombre,terminado}){
//     console.log("ID:",id);
//     console.log("Nombre:",nombre);
//     console.log("Terminado?:",terminado?"SI":"NO");
    
// }

//07spread

//07 Spread
//Sin uso de spread
// function add(){
//     //Arguments contiene los parámtros
//     var values = Array.prototype.splice.call(arguments,[0]);
//     var total = 0;

//     for(var value of values) {
//         total +=value;
//     }

//     return total;
// }
// //OJO: Esto manda error de TS pero no es problema en JS
// console.log("Suma ES5 style:",add(1,2,3,4,5));

// function addWithSpread(...values){
//     var total = 0;
//     for(var value of values) {
//         total +=value;
//     }

//     return total;   
// }

// console.log("Suma ES6 Style",addWithSpread(1,2,3,4,5));

//Incluyendo argumentos adicionales al spread
// function calculate(action,...values){
//     var total = 0;
//     for(var value of values) {

//         switch(action){
//             case "suma": 
//             total +=value;
//             break;
//             case "resta":
//             total-=value;
//             break;
//         }
        
//     }

//     return total;
// }

// console.log("Operacion Resta",calculate("resta",1,2,3,4,5));
// console.log("Operacion Suma",calculate("suma",1,2,3,4,5));

//uso para mezclar arreglos
// var arr1 = [2,3,4];
// var arr2 = [0,1,...arr1,5,6,7];

// console.log("Arreglo mezclado",arr2);

// //Uso para agregar al final de un arreglo
// var adicionales = [8,9,10];

// arr2.push(...adicionales);

// console.log("Arreglo Aumentado",arr2);

// //renombrar archivo a .ts para ver como transpila a ES5


//08computedprops

// let domtype = "particular";

// var persona = {
//   nombre: "Roberto",
//   id: 234,
//   ["domicilio_"+domtype]: {
//     "calle y num": "Norte 45 No. 1190",
//     colonia: "Industrial Vallejo",
//     cp: "03600"
//   }
// }
// console.log(`Nombre: ${persona.nombre}. Domicilio: ${persona.domicilio_particular["calle y num"]}, col. ${persona.domicilio_particular.colonia} cp ${persona.domicilio_particular.cp}`);

// console.log(`Array notation. Nombre: ${persona['nombre']}. 
// Domicilio: ${persona['domicilio_particular']["calle y num"]}, col. ${persona['domicilio_particular']['colonia']} cp ${persona.domicilio_particular.cp}`);


// console.log(persona["domicilio_"+domtype].colonia);


// var phoneType = "work";
// var myObject = {
//     nombre: "Ricardo",
//     [phoneType+"Phone"]:"57880990"
// }

// console.log("Object with Computed Properties",myObject);


//04 tipos

// var duck = {
//     name: "Donald",
//     species: "ducks",
//     age: 2,
//     quack: function () {
//         console.log("Quack!");
//     }
// }

// function makeAnimalSpeak(animal) {
//     animal.speak();
// }
// makeAnimalSpeak(duck);
// //Ejemplo de validacionde inferencia
// duck.name = 234;

// var otherDuck = {
//     name: "Donald",
//     species: "ducks",
//     age: calcularEdad(2015),
//     quack: function () {
//         console.log("Quack!");
//     }
// }
// function calcularEdad(anioNacimiento){
//     return new Date().getFullYear() + anioNacimiento;
// }

// //Limitantes
// function calculaTotalLength(x,y){
//     //TS no sabe el tipo porque el '+' se usa p suma o concatenación
//     //let total = x.length + y.length;
//     //TS no indica error si no sabe la estructura de los objetos
//     //let total = x.length + y.leng;
//     //TS infiere que son numeros porque el '-' solo se usa en numeros.
//     let total = x.length - y.length;
//     return total;
// }

//02Tipos explicitos
//tipo en param2
//tipo en variable de retorno
//tipo en valor de retirno declarado en firma de funcion
//tipo en param1 (array de cualquier tipo)
// function calculateTotalLength(x:any[],y:string):number{    
//     let total:number = x.length + y.length;    
//     return total;
// }

//

//Union types
//string | any[]
// function calcularTheTotalLength(x:string | any[],y:string | any[]):number{    
//     let total:number = x.length + y.length;
//     //OJO: TS manda error si al declarar comando específicos 
//     //solo disponibles para un tipo a menos que se usen condicionales

//     x.slice(0);

//     //Type guard syntax

//     // //solo si es arreglo
//     // x.push('hola');
//     // //solo si es string
//     // x.substr(1);   
    
//     if(x objetoof Array){
//         x.push('hola');
//     }

//     if(typeof x === "string"){
//         x.substr(1);
//     }

//     return total;
// }

// function calculateATotalLength(x:string,y:string):number
// function calculateATotalLength(x:string,y:any[]):number
// function calculateATotalLength(x:any[],y:any[]):number
// function calculateATotalLength(x:string|any[],y:any[]|string):number{    
//     let total:number = x.length + y.length;
//     //OJO: TS manda error si al declarar comando específicos 
//     //solo disponibles para un tipo a menos que se usen condicionales

//     x.slice(0);
    
//     if(x objetoof Array){
//         x.push('hola');
//     }

//     if(typeof x === "string"){
//         x.substr(1);
//     }

//     return total;
// }
// //TS muestra las 2 opciones de sobrecarga
// let totalLength = calculateATotalLength("Hola","Mundo");
// console.log("Longitud total:"+ totalLength);


//
// class Persona {
//   id:number;
//   nombre:string;
//   apellidos: string;
//   edad:number;
// }

// class Tarea {
//   id: number;
//   nombre: string;
//   finalizada: boolean;
// }

// let persona:Persona = new Persona();

// persona.id = 45;
// persona.nombre = "Ruben";
// persona.apellidos = "Rosas";
// persona.edad = 35;

// console.log(`la edad de ${persona.nombre} ${persona.apellidos} es ${persona.edad}`);

//mandatory properties (id, name, completed)
//Optional properties name?
// interface Tarea {
//   id:number;
// }

// var myTarea:Tarea = {
//   id: 123
// }
// var myTod = <Tarea> {}

// //add,delete,getAll,getbyId
// /*
//     add(todo:Tarea):Tarea;
//     delete(todoId:number):void;
//     getAll():Tarea[];
//     getById(todoId:number):Tarea;
//  */
// interface ITareaService {
//     add(todo:Tarea):Tarea;
//     delete(todoId:number):void;
//     getAll():Tarea[];
//     getById(todoId:number):Tarea;
// }

//Interface para usar con funciones
//OJO: Sólo debe definirse una firma de funcion anonima en la interface
//De lo contrario el complidaor se confunde.
//Pueden agregarse propiedades adicionales que no sean una firma de funcion.
// interface SearchFunc {
//     (source: string, searchString: string): boolean;
//     //OJO: Solo debe usarse una defincion, sino TS se confunde
//     // (other:number, more:string):boolean;
// }

// let mySearch: SearchFunc = function(src: string, sub: string):boolean {
//     let result = src.search(sub);
//     return result > -1;
// }

// //Usando casting
// let myOtherSearch = <SearchFunc>function(src: string, sub: number) {
//     let result = src.search(sub);
//     return result > -1;
// }

//150000




//07 extending interface
// interface Persona {
//     nombre:string;
//     apellidos:string;
//     aNacimiento:number;
// }
// //OJO: Esto solo se recomienda hacer para objetos externos
// //ya que no tiene mucho sentido hacerlo si se tiene acceso a la interface original.
// interface Persona {
//     calcularEdad():number;
// }

// var person:Persona = {
//     nombre:"Roberto",
//     apellidos:"Rosales",
//     aNacimiento:1999,
//     calcularEdad:function(){
//         return new Date().getFullYear() - this.aNacimiento;
//     }
// }

// console.log(`${person.nombre} ${person.apellidos}. Edad: ${person.calcularEdad()}`);

//08enums

// enum EdoTarea {
//     Nueva = 1,
//     Activa = 2,
//     Completada = 3,
//     Eliminada = 4
// }

// interface Tarea {
//     id:number;
//     nombre:string;
//     terminado?:boolean;
//     state: EdoTarea; //

// }

// var aTarea: Tarea = {
//     id: 1345,
//     nombre: "Desayunar",
//     state: EdoTarea.Nueva  
// } 

// function eliminar(tarea:Tarea){
//     if(tarea.state != EdoTarea.Completada) {
//         throw "No se puede eliminar una tarea que no ha sido completada!";
//     } else {
//       console.log("Tarea eliminada! :D");
//     }
// }

// eliminar(aTarea);



//09anontypes

//Los tipos anónimos no definen la estructura como I o C, sino como OLiteral
// let unaTarea: {nombre:string};
// unaTarea = {
//     edad: 4
// }

// function calculateTheTotalLength(x:any[]|string,y:any[]|string):number{    
//     let total:number = x.length + y.length;

//     return total;
// }
//En lugar de usar uniones, se puede usar un anon type para definir el tipo mínimo esperado (que tenga length, por ej.)
//OJO: Esto no evita que x sea un array y y un string, por ejemplo.
//Cuando se vean generics se podrá ver como manejarlo.
// function calculaTheTotalLength(x:{length:number},y:{length:number}):number{    
//     let total:number = x.length + y.length;

//     return total;
// }

// console.log(calculaTheTotalLength([1,2,3,45],["Hola"]));

// - - - - - 
//05 Clases
// - - - - -
//01protoinheritance
//Object.prototype;
// var todo = {}; //new Object()

// function TaskService() {
//     this.todos = ["Levantarse","Bañarse", "Desayunar"];
// }  
// TaskService.prototype.getAll = function (){
//     return this.todos;
// }

// var taskService = new TaskService();

// for(let tarea of taskService.getAll()) {
//   console.log(tarea);
// }

//02classdefinition

enum EdoTarea {
    Nueva = 1,
    Activa = 2,
    Completada = 3,
    Eliminada = 4
}

// interface Tarea {
//     id:number;
//     nombre:string;
//     terminado?:boolean;
//     state?: EdoTarea; //

// }

// class TareaService {
//     //declaracion de propiedad, opcion 1. Inferencia de tipos
//     //todos;
//     //opcion 2. declaracion explícita de tipos
//     //todos:Tarea[];

//     //opcion 3. declaración con valor incial
//     // todos:Tarea[] = [];

//     // constructor(){       

//     //     // this.todos = [];
//     // }

//     //opcion 4. declaracion directa en el constructor
//     //usando modificador de acceso

//     constructor(private todos:Tarea[]){

//     }

//     getAll(){
//         return this.todos;
//     }
// }

// let miTareaService = new TareaService([
//   {id:1,nombre:"Levantarse"},
//   {id:2, nombre:"Bañarse"},
//   {id:3, nombre:"Desayunar"}
// ]);

// miTareaService.todos.push({id:4, nombre:"Cepillarse"});

// for(let tarea of miTareaService.getAll()){
//   console.log(tarea.nombre);
// }

//03propiedadesestaticas
//ES5
// function TodoService() {}
// TodoService.lastId = 0;

// TodoService.getNextId = function() {
//   return (TodoService.lastId += 1);
// };

// TodoService.prototype.add = function(todo) {
//   var newId = TodoService.getNextId();
//   console.log("ID nuevo:",newId);
// };

//TS

// class TodoService {
//   static lastId: number = 0;

//   constructor(private todos: Tarea[]) {}

//   static getNextId() {
//     return (TodoService.lastId += 1);
//   }

//   add(todo: Tarea){
//       var newId = TodoService.getNextId();
//       console.log("ID nuevo:",newId);
//   }

//   getAll() {
//     return this.todos;
//   }
// }

// let myService = new TodoService(
//   [
//   {id:1,nombre:"Levantarse"},
//   {id:2, nombre:"Bañarse"},
//   {id:3, nombre:"Desayunar"}
// ]);
// myService.add({id:5,nombre:"Tarea 1"});
// let myService2 = new TodoService([
//   {id:1,nombre:"Levantarse"},
//   {id:2, nombre:"Bañarse"},
//   {id:3, nombre:"Desayunar"}
// ]);
// myService2.add({id:6,nombre:"Tarea 2"});
// let myService3 = new TodoService([
//   {id:1,nombre:"Levantarse"},
//   {id:2, nombre:"Bañarse"},
//   {id:3, nombre:"Desayunar"}
// ]);
// myService3.add({id:7, nombre:"Tarea 3"});

//04accesores
// var laTarea = {
//   nombre: "Desayunar",
//   _edo:EdoTarea.Nueva,
//   get estado() {
//     // return EdoTarea.Completada;
//     return this._edo;
//   }
//   ,
//   set estado(nuevoEdo) {
//     //Se crea una propiedad interna donde llevar el valor.
//     this._edo = nuevoEdo;
//   }
//   // set estado(nuevoEdo) {
//   //     if(nuevoEdo === EdoTarea.Completada){
//   //         let puedeCompletarse = this.estado == EdoTarea.Activa || this.estado == EdoTarea.Eliminada;

//   //         if(!puedeCompletarse) {
//   //             throw "Una Tarea debe estar Activa o Eliminada para poder Completarse";
//   //         }

//   //     }
//   // }

//   //OJO: Sin setter no se puede modificar el edo de la tarea externamente
// };
// laTarea.estado = EdoTarea.Eliminada;

// console.log(laTarea.estado);
// console.log(laTarea._edo);  


//Usando clases

class Tarea {
  _edo?:EdoTarea;
  nombre: string;
  id:number;

  constructor(nombre:string){
    this.nombre = nombre;
    this._edo = EdoTarea.Nueva;
  }

  get estado() {
    // return EdoTarea.Completada
    return this._edo;
  }

  set estado(nuevoEdo) {
    if (nuevoEdo === EdoTarea.Completada) {
      let puedeCompletarse =
        this.estado == EdoTarea.Activa || this.estado == EdoTarea.Eliminada;

      if (!puedeCompletarse) {
        throw "Una Tarea debe estar Activa o Eliminada para poder Completarse";
      }
      this._edo = nuevoEdo;
      console.log("Tarea Completada! :D");
    } else {
      this._edo = nuevoEdo;
      console.log("Estado actualizado! :)");
    }
  }
}

// let tarea = new Tarea("Desayunar");

// tarea.estado = EdoTarea.Activa;
// tarea.estado = EdoTarea.Completada;


//05behaviorinheritance

//Uso de patrón State
// class TareaStateChanger {
//     constructor(private nuevoEdo:EdoTarea){

//     }
//     puedeCambiarEdo(tarea:Tarea):boolean {
//         return !!tarea;
//     }

//     cambiarEdo(tarea:Tarea):Tarea {
//         if(this.puedeCambiarEdo(tarea)){
//             tarea.estado = this.nuevoEdo;
//         }
//         return tarea;
//     }
// }

// class TareaStateChangerCompleto extends TareaStateChanger {
//     constructor(){
//         super(EdoTarea.Activa)
//     }

//     puedeCambiarEdo(tarea:Tarea):boolean{
//         return super.puedeCambiarEdo(tarea) && (tarea.estado == EdoTarea.Activa || tarea.estado == EdoTarea.Eliminada);
//     }
// }

//06abstractclasses
// abstract class TareaStateChangerAbs {
//     constructor(private /*protected*/ nuevoEdo:EdoTarea){

//     }
//     abstract puedeCambiarEdo(tarea:Tarea):boolean;

//     cambiarEdo(tarea:Tarea):Tarea {
//         if(this.puedeCambiarEdo(tarea)){
//             tarea.estado = this.nuevoEdo;
//         }
//         return tarea;
//     }
// }

// class TareaCambiadorEdoCompleto extends TareaStateChangerAbs {
//     constructor(){
//         super(EdoTarea.Activa)
//     }

//     puedeCambiarEdo(tarea:Tarea):boolean{
//         return !!tarea && (tarea.estado == EdoTarea.Activa || tarea.estado == EdoTarea.Eliminada);
//     }
// }

//07modifsacceso
// class TareaService {
//   /*private */static lastId: number = 0;
  
//   //una vez que se guarda el valor al construirse, no es accesible fuera
//   constructor(private todos: Tarea[]) {}

//   /*private*/ static getNextId() {
//     return (TareaService.lastId += 1);
//   }

//   add(todo: Tarea) {
//     var newId = TareaService.getNextId();
//   }

//   getAll() {
//     return this.todos;
//   }
// }

// let tareaService = new TareaService([
//   {id:1, nombre:"Levantarse",estado:1},
//   {id:2, nombre:"Bañarse",estado:1},
//   {id:3, nombre:"Desayunar",estado:1}
// ]);

// tareaService.todos.push([{id:4, nombre:"Salir",estado:1}]);

//Accesores solo aplican en tiempo de desarrollo, no en runtime
//uso de convenciones (_privProp)

//08implinterfaces

// interface ITareaService {
//     add(todo:Tarea):Tarea;
//     delete(todoId:number):void;
//     getAll():Tarea[];
//     getById(todoId:number):Tarea;
// }

// interface IIdGenerator {
//     nextId:number;
// }

// class TareaService implements ITareaService, IIdGenerator {
//   nextId: number;
//   private static lastId: number = 0;

//   //una vez que se guarda el valor al construirse, no es accesible fuera
//   constructor(protected tareas: Tarea[]) {}

//   // private static getNextId() {
//   //     return (TareaService.lastId += 1);
//   // }
//   static get nextId(): number {
//     return (TareaService.lastId += 1);
//   }

//   add(tarea: Tarea) {
//     // var newId = TareaService.getNextId();
//     tarea.id = TareaService.nextId;
//     this.tareas.push(tarea);

//     return tarea;
//   }

//   getAll(): Tarea[] {
//     return this.tareas;
//     //Devuelve copia de listado para evitar que sean cambiadas fuera
//     // let clone = JSON.stringify(this.tareas);
//     // return JSON.parse(clone);
//   }

//   getById(idTarea: number) {
//     var filtered = this.tareas.filter(tarea => tarea.id == idTarea);

//     if (filtered.length) {
//       return filtered[0];
//     }

//     return null;
//   }

//   delete(idTarea: number) {
//     let tareaAEliminar = this.getById(idTarea);
//     let indexAEliminar = this.tareas.indexOf(tareaAEliminar);
//     this.tareas.splice(indexAEliminar, 1);
//   }
// }


//S05

//0835

//- - - - - 
//06generics 
//- - - - -
//01genericsIntro

// function clone<T>(value:T):T {
//     console.log(value);
//     let miOtroValor = value;

//     //value = {saludo: "Hello",nombre:"Ricardo"};

//     let serialized = JSON.stringify(value);    
//     console.log(serialized);
//     let parsed = JSON.parse(serialized);
//     console.log(parsed);
//     if(value === miOtroValor){
//       console.log("Iguales!")
//     } else {
//       console.log("NO SON IGUALES!");
//     }
//     return parsed;
// }


// // clone<>({saludo:'Hello', nombre:"Ricardo"/*, edad:function(){return 20}*/});
// clone(1234);
// clone<string>(1234);
// let persona = {saludo:'Hello', nombre:"Ricardo"/*, edad:function(){return 20}*/};
// interface Todo {
//     id:number,
//     name: string,
//     active: boolean
// }
// var task: Todo = {
//     id: 23,
//     name: "Rosa",
//     active: true
// }

// clone<Todo>(persona);
// clone({other:"anon object"});
//02genericsClasses

//02 Generics Classes
// var array: number[] = [1,2,3];

// class MyMap<K,V> {
//     constructor(
//         public key: K,
//         public value: V
//     ){

//     }
// }

// //Types inferidos
// let otherMap = new MyMap("date",new Date());
// let othermap2 = new MyMap("date",Date.now());

// // Types explicitos
// let mykvMap = new MyMap<number,string>(1,"nombre");
// let mykvMap2 = new MyMap<string,number>("edad", 23);

// //intellisense según el tipo declarado :)
// // mykvMap.value.

// var array2:Array<number> = [1,2,3];

// class MapPrinter<K,V>{
//     constructor(private map2Print:MyMap<K,V>[] ){

//     }
//     print() {
//         for(let element of this.map2Print){
//             console.log(`${element.key}: ${element.value}`);
//         }
//     }
// }
// //OJO: No puede inferir los tipos K,V porque cada map usa distintos
// // var printer = new MapPrinter([otherMap,othermap2,mykvMap]);

// var printerOk = new MapPrinter([othermap2,mykvMap2]);
// printerOk.print();

//03genericsConstraints

//Cualquier parametero que tenga propiedad length
// function totalLength(x:{length:number}, y:{length:number}){
//     var total:number = x.length + y.length;
//     return total;
// }

// function totalLength<T>(x:T, y:T){
//     var total:number = x.length + y.length;
//     return total;
// }

//generic constraint
// function totalLength<T extends {length:number}>(x:T, y:T){
//     var total:number = x.length + y.length;
//     return total;
// }

// generic constraint using an interface
// interface IWithLength {
//     length: number
// }
// function totalLength<T extends IWithLength>(x:T, y:T){
//     var total:number = x.length + y.length;
//     return total;
// }

// //Extraño pero válido (suma de longitudes de cadena y un arreglo :P)
// var length = totalLength<string>("Hola","[1,2,3]");

// //OK. suma de longitudes de 2 objetos del mismo tipo
// // var lentghOk = totalLength("Hola", "mundo");

// //OJO: Al usar herencia, cualquier descendiente del Type es un objeto válido
// //por lo que pueden sumarse los 2 tipos aunque sean distintos (por ser parientes :P)
// class MyArray<T> extends Array<T> {
// }
// var otherLength = totalLength<MyArray<number>>([1,2,3],new MyArray<number>(1,2,3,4,5));


//- - - - - 
//07decorators
//- - - - - 
//01methodDecorator
// class MyObj {
//     @log 
//     miMetodo(name:string): string {
//         console.log("Hola "+name);
//         return "OK";
//     }
// }

// //Decorador para logging de méthod
// function log(target: object, methodName:string, descriptor:TypedPropertyDescriptor<Function>){
//     //El método en sí.
//     var originalMethod = descriptor.value;
//     descriptor.value = function(...args){
//         console.log(`Se invocó ${methodName} cona rgumentos: ${JSON.stringify(args)}`);
//         let returnValue = originalMethod.apply(this,args);
//         console.log(`Resultado: ${returnValue}`);

//         return returnValue;
//     }
// }

// var myObj = new MyObj();
// myObj.miMetodo("Loco");

//0234classPropFactoryDecorator
// interface Todo {
//   id: number;
//   name: string;
//   active: boolean;
// }
// //02 Class Decorators
// @validatable
// class ValidatableTodo implements Todo {
//   id: number;
//   @required 
//   //OJO: Ver que se invoca la funcion y el resutltado es otra función, el decorador per se, pero al ser un closure se puede usar el param
//   @regex(`^[a-zA-Z]*$`)
//   name: string;
//   active: boolean;
// }

// //02.1
// //OJO: Al parecer el que se llame igual que la clase anterior solo es por practicidad. Podría llamarse diferente, ya que no se relacionan.
// //Ya que en realidad lo que está haciendo es definir que la clase
// //que realice esta interfaz debe implementar también el método validate()
// //OJO: Al parecer SI SE RELACIONAN con lo cual se une el hecho de que
// //la clase Validatable implementa Todo y con eso se forza a que se implementen
// //las propiedades de la interface Todo
// interface ValidatableTodo extends IValidatable {}

// interface IValidatable {
//   validate(): IValidationResult[];
// }  

// interface IValidationResult {
//   isValid: boolean;
//   message: string;
//   property?: string;
// }

// interface IValidator {
//   (objeto: Object): IValidationResult;
// }

// function validate(): IValidationResult[] {
//   let validators: IValidator[] = [].concat(this._validators);
//   // console.log(validators);
//   let errors: IValidationResult[] = [];
//   for (let validator of validators) {
//     let result = validator(this);
//     if (!result.isValid) {
//       errors.push(result);
//     }
//   }
//   errors.length? 
//   console.error("Errores:", errors): 
//   console.log("Sin errores al validar :)");
//   return errors;
// }

// //OJO: Indica error si no está declarada la propiedad en la clase. Por eso se tiene que definir la interface IValidatable
// //Usando decoradores, esta forma de agregar la funcion validate, ya no se necesita, sino que se usa el decorator
// // ValidatableTodo.prototype.validate = validate;

// //Agrega el método validate al objeto target de tipo Function
// //OJO: En un momento dado se podrían agregar también interceptores a todos los métodos del Function
// //siempre y cuando se conozcan sus nombres o se obtengan procesando las properties de dicha Function
// function validatable(target: Function) {
//   target.prototype.validate = validate;
// }

// var otro = new ValidatableTodo();
// otro.name = "Ricardo";
// otro.validate();


// function required(target: Object, propertyName: string) {
//   //OJO: Aqui hace un 'cast' de target a un objeto que se espera
//   //cumpla con la interfaz anónima que tenga la propiedad _validators, 
//   //pero no es algo forzoso, sino más bien esperado.
//   let validatable = <{ _validators: IValidator[] }>target;
//   //Se asegura que de no estar definida la propiedad _validators se inicialice con un arreglo en blanco.
//   let validators = validatable._validators || (validatable._validators = []);
//   //OJO: la función que se agrega a validators es la que se ejecuta en el for (let validator of validators) de validate()
//   validators.push(function(objeto) {
//     let propertyValue = objeto[propertyName];
//     let isValid = propertyValue != undefined;

//     if (typeof propertyValue === "string") {
//       isValid = propertyValue && propertyValue.length > 0;
//     }

//     return {
//       isValid,
//       message: `${propertyName} es requerido`,
//       property: propertyName
//     };
//   });
// }

// //04 Decorator factories
// //Permiten pasar parámetros a los decoradores, adicionales a la firma esperada del decorador, la cual no puede cambiar.
// //OJO: Ver que se invoca la funcion y el resutltado es otra función
// function regex(pattern: string) {

//     let expression = new RegExp(pattern);
//   return function regex(target: Object, propertyName: string) {
//     //OJO: Aqui hace un 'cast' de target a un objeto que se espera
//     //cumpla con la interfaz anónima que tenga la propiedad _validators, pero no es algo forzoso, sino más bien esperado.
//     let validatable = <{ _validators: IValidator[] }>target;
//     //Se asegura que de no estar definida la propiedad _validators se inicialice con un arreglo en blanco.
//     let validators = validatable._validators || (validatable._validators = []);
//     // if(validatable._validators !== undefined){
//     //   validators = validatable._validators;
//     // } else {
//     //   validators._validators = [];
//     // }

//     validators.push(function(objeto) {
//       let propertyValue = objeto[propertyName];
//       let isValid = expression.test(propertyValue);

//       // if (typeof propertyValue === "string") {
//       //   isValid = propertyValue && propertyValue.length > 0;
//       // }

//       return {
//         isValid,
//         message: `${propertyName} no cumple con el patrón ${expression}`,
//         property: propertyName
//       };
//     });
//   };
// }

// 1030





//- - - - - 
//08modules 
//- - - - -

//01modulepattern
//IIFE
//Immediately Invoqed  Function Expression
// (function() {})();

// var testModule = (function() {
//   var contador = 0;

//   return {
//     incrementaContador: function() {
//       console.log(`Valor de contador antes de incrementar: ${contador}`);
//       return contador++;
//     },

//     resetContador: function() {
//       console.log("valor de contador antes de reiniciarlo: " + contador);
//       contador = 0;
//     }
//   };
// })();

// //

// // Incrementar contador
// testModule.incrementaContador();
// testModule.incrementaContador();

// testModule.resetContador();

//Simulación de Clases

// var Person = (function(firstName, lastName, email) {
//     //OJO: Esta variable privada sería equivalente a un static property
//     var miVariablePrivada = "No me ven";    
    
//     var Persona = function(nombre,apellidos,correo,aNacimiento){
//         this.nombre = nombre;
//         this.apellidos = apellidos;
//         this.correo = correo;
//         this.aNacimiento = aNacimiento;
//     }

//     Persona.prototype.calcularEdad = function(){
//         return new Date().getFullYear() - this.aNacimiento;
//     }

//     Persona.prototype.verMiVariablePrivada = function(){
//         return miVariablePrivada + " a simple vista ;)";
//     }

//     return Persona;
// //OJO: los parametros que se le pasen a la IIFE se pueden ver como  las dependencias externas. Es la forma de pasarle objetos al modulo.
// })();

import Person from "./Persona";

var aPerson = new Person("Ricardo","Antonio","mogarick@gmail.com",1979);
//aPerson.miVariablePrivada = "Si te veo";
// console.log("aPerson.miVariablePrivada",aPerson.miVariablePrivada);
//OJO: No está modificando miVariablePrivada, 
//sino definiendo la variable miVariablePrivada para la instancia aPerson
// aPerson.miVariablePrivada = "Si te veo";
// console.log("aPerson.miVariablePrivada",aPerson.miVariablePrivada);

var anotherPerson = new Person("Ramiro","Rosales","coprreo@gmail.com",1989);
// console.log("anotherPerson.miVariablePrivada",anotherPerson.miVariablePrivada);

console.log("aPerson.miVariablePrivada",aPerson.verMiVariablePrivada());
console.log("anotherPerson.miVariablePrivada",anotherPerson.verMiVariablePrivada());


//02import
// import Person from "./Persona";
